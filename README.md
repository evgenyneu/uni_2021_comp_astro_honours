## 2021 computation astrophysics honours

My logbook for the Monash unit. The notes on individual days are in Month/Day directories, each number prepended with letter 'a' to allow running Python modules. For example, notes for 22th Feb are located in a02/a22 directory.
